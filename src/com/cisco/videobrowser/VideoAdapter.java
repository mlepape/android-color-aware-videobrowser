package com.cisco.videobrowser;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

@SuppressLint("NewApi")
public class VideoAdapter extends BaseAdapter {

	private static final String TAG = "VideoBrowser.VideoAdapter";
	private static final String ns = null;

	public static class Video {
		public String videoUrl;
		public String name;
		public String snapshotUrl;
		TextView nameView = null;
		TextView urlView = null;
		TextView classView = null;
		ProgressBar snapshotProgressView = null;
		ImageView snapshotView = null;
		
		Video() {
			videoUrl = null;
			name = null;
			snapshotUrl = null;
		}
		
		Video(XmlPullParser parser) throws XmlPullParserException, IOException {
			parser.require(XmlPullParser.START_TAG, ns, "video");
			while (parser.next() != XmlPullParser.END_TAG) {
				if (parser.getEventType() != XmlPullParser.START_TAG) {
					continue;
				}
				String name = parser.getName();
				if (name.equals("url")) {
					readVideoUrl(parser);
				} else if (name.equals("name")) {
					readName(parser);
				} else if (name.equals("snapshot")) {
					readSnapshotUrl(parser);
				} else {
					XmlParserUtils.skip(parser);
				}
			}
		}
		
		private void readVideoUrl(XmlPullParser parser) throws XmlPullParserException, IOException {
			videoUrl = XmlParserUtils.readTextFromTag(parser, ns, "url");
		}
		
		private void readName(XmlPullParser parser) throws XmlPullParserException, IOException {
			name = XmlParserUtils.readTextFromTag(parser, ns, "name");
		}
		
		private void readSnapshotUrl(XmlPullParser parser) throws XmlPullParserException, IOException {
			snapshotUrl = XmlParserUtils.readTextFromTag(parser, ns, "snapshot");
		}
		
		void update(Video v, Context context) {
			v.name = name;
			v.videoUrl = videoUrl;
			v.snapshotUrl = snapshotUrl;
			v.nameView.setText(name);
			v.urlView.setText(videoUrl);
			try {
				v.classView.setText(String.format(context.getString(R.string.prefix_class), ColorPolicy.currentPolicy.getColorForUri(videoUrl), ColorPolicy.currentPolicy.applyPolicy(videoUrl)));
			} catch (URISyntaxException e) {
				Log.e(TAG, "Uri syntax exception while showing video class.", e);
			}
			new DownloadImageTask(v).execute(snapshotUrl);
		}
	}

	private static class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		Video video;

		public DownloadImageTask(Video video) {
			this.video = video;
			video.snapshotProgressView.setIndeterminate(true);
			video.snapshotProgressView.setVisibility(View.VISIBLE);
			video.snapshotView.setVisibility(View.GONE);
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			video.snapshotView.setImageBitmap(result);
			video.snapshotProgressView.setVisibility(View.GONE);
			video.snapshotView.setVisibility(View.VISIBLE);
		}
	}

	private Context context;
	private ArrayList<Video> videos;
	
	private void parseXmlList(InputStream in) throws XmlPullParserException, IOException {
		XmlPullParser parser = Xml.newPullParser();
		parser.setInput(in, null);
		
		while(parser.getEventType() != XmlPullParser.START_TAG) {
			parser.next();
		}

		parser.require(XmlPullParser.START_TAG, ns, "videos");
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			if (name.equals("video")) {
				videos.add(new Video(parser));
			} else {
				XmlParserUtils.skip(parser);
			}
		}
	}
	
	public VideoAdapter(Context context) {
		this.context = context;
		videos = new ArrayList<VideoAdapter.Video>();
	}
	
	public VideoAdapter(Context context, String url) throws XmlPullParserException, IOException, URISyntaxException {
		this.context = context;
		videos = new ArrayList<VideoAdapter.Video>();
		HttpURLConnection connection = null;
		try {
			URL coloredUrl = new URL(ColorPolicy.currentPolicy.applyPolicy(url));
			connection = (HttpURLConnection)coloredUrl.openConnection();
		} catch(IOException e) {
			Log.e(TAG, "Unable to open connection to video list server", e);
			throw e;
		} catch (URISyntaxException e) {
			Log.e(TAG, "URI syntax exception while fetching video list", e);
			throw e;
		}
		
		try {
			parseXmlList(connection.getInputStream());
		} catch (IOException e) {
			Log.w(TAG, "Unable to obtain video list", e);
			throw e;
		} catch (XmlPullParserException e) {
			Log.w(TAG, "Unable to parse video list", e);
			throw e;
		} finally {
			connection.disconnect();
		}
	}

	@Override
	public int getCount() {
		return videos.size();
	}

	@Override
	public Video getItem(int index) {
		return videos.get(index);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(context);
		Video videoTag = null;
		if(convertView == null || (convertView.getTag() instanceof Video)) { // Cannot reuse view
			convertView = inflater.inflate(R.layout.row_layout, null);
			videoTag = new Video();
			videoTag.nameView = (TextView)convertView.findViewById(android.R.id.text1);
			videoTag.urlView = (TextView)convertView.findViewById(android.R.id.text2);
			videoTag.classView = (TextView)convertView.findViewById(R.id.text3);
			videoTag.snapshotView = (ImageView)convertView.findViewById(R.id.snapshot);
			videoTag.snapshotProgressView = (ProgressBar)convertView.findViewById(R.id.snapshot_progress);
			convertView.setTag(videoTag);
		}
		else {
			videoTag = (Video) convertView.getTag();
		}
		Video video = (Video)getItem(position);
		video.update(videoTag, context);
		return convertView;
	}

}
