package com.cisco.videobrowser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONStringer;
import org.xmlpull.v1.XmlPullParserException;

import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

@SuppressLint("NewApi")
public class MainActivity extends ListActivity {

	private static final String TAG = "VideoBrowser.MainActivity";

	private String encodedAddresses = null;

	private class ObtainPolicyTask extends AsyncTask<URL, Integer, ColorPolicy> {
		
		ProgressDialog dialog;
		Context context;
		
		public ObtainPolicyTask(Context context) {
			this.context = context;
		}

		@Override
		protected ColorPolicy doInBackground(URL... params) {
			int count = params.length;
			if (count > 1) {
				throw new IllegalArgumentException("ObtainPolicyTask can only obtain a single policy");
			}
			SparseArray<HashSet<Inet6Address>> addresses = getAddressesByColor();
			if(addresses.size() == 0) {
				// TODO Print error message
				Log.i(TAG, "No IPv6 addresses available.");
				return null;
			}

			// Encode addresses
			encodedAddresses = jsonEncodeAddresses(addresses);
			if(encodedAddresses == null) { // This shouldn't happen
				// TODO Print error message (Logcat message already printed in jsonEncode)
				return null;
			} else {
				Log.d(TAG, encodedAddresses);
			}
			try {
				return new ColorPolicy(params[0], encodedAddresses);
			} catch (IOException e) {
				return null;
			} catch (XmlPullParserException e) {
				return null;
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(context);
			dialog.setMessage(getApplicationContext().getText(R.string.fetching_policy));
			dialog.setIndeterminate(true);
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected void onPostExecute(ColorPolicy result) {
			ColorPolicy.currentPolicy = result;
			dialog.dismiss();
			if(result != null) {
				SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
				String list_url = preferences.getString("list_url", "http://video.maicolepape.org/list");
				new ObtainVideoListTask(context).execute(list_url);
			}
		}
	}

	private class ObtainVideoListTask extends AsyncTask<String, Integer, VideoAdapter> {
		
		ProgressDialog dialog;
		Context context;
		
		public ObtainVideoListTask(Context context) {
			this.context = context;
		}

		@Override
		protected VideoAdapter doInBackground(String... params) {
			int count = params.length;
			if (count > 1) {
				throw new IllegalArgumentException("ObtainVideoListTask can only obtain a single video list");
			}
			try {
				return new VideoAdapter(getApplicationContext(), params[0]);
			} catch (IOException e) {
				return null;
			} catch (XmlPullParserException e) {
				return null;
			} catch (URISyntaxException e) {
				return null;
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(context);
			dialog.setMessage(getApplicationContext().getText(R.string.fetching_videos));
			dialog.setIndeterminate(true);
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected void onPostExecute(VideoAdapter result) {
			dialog.dismiss();
			if(result != null) {
				setListAdapter(result);
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onStart() {
		super.onStart();

		try {
			SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			String policy_url_string = preferences.getString("policy_url", "http://video.maicolepape.org/policy");
			URL policy_url = new URL(policy_url_string);
			new ObtainPolicyTask(this).execute(policy_url);
		} catch (java.net.MalformedURLException e) {
			Log.e(TAG, "Policy url malformed: ", e);
		}
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		VideoAdapter.Video video = (VideoAdapter.Video)l.getAdapter().getItem(position);
		String url = null;
		try {
			url = ColorPolicy.currentPolicy.applyPolicy(video.videoUrl);
		} catch (URISyntaxException e) {
			Log.e(TAG, "URI syntax error when launching the video", e);
			return;
		}
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		Log.d(TAG, "Launching intent for " + url);
		startActivity(intent);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		if(item.getItemId() == R.id.action_settings) {
			startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
		}
		return super.onMenuItemSelected(featureId, item);
	}

	private SparseArray<HashSet<Inet6Address>> getAddressesByColor() {
		HashSet<Inet6Address> addresses = new HashSet<Inet6Address>();
		SparseArray<HashSet<Inet6Address> > addressesByColor = new SparseArray<HashSet<Inet6Address> >();

		// Check for DHCPv6 obtained addresses
		File directory = new File("/data/dhcp");
		Pattern pattern = Pattern.compile("dhcpv6_.*\\.leases");
		for(File file: directory.listFiles()) {
			Matcher matcher = pattern.matcher(file.getName());
			if(matcher.matches()) {
				try {
					BufferedReader reader = new BufferedReader(new FileReader(file));
					String line;
					while((line = reader.readLine()) != null) {
						String[] fields = line.split(" ");
						Inet6Address address = (Inet6Address)InetAddress.getByName(fields[0]);
						int color = Integer.parseInt(fields[3]);
						HashSet<Inet6Address> addressesForColor = addressesByColor.get(color);
						if(addressesForColor == null) {
							addressesForColor = new HashSet<Inet6Address>();
						}
						addressesForColor.add(address);
						addressesByColor.put(color, addressesForColor);
						addresses.add(address);
					}
					reader.close();
				}
				catch (IOException e) {
					Log.e(TAG, "IOException while reading lease file.", e);
				}
			}
		}

		// Add addresses obtained by RAs
		try {
			Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
			for(NetworkInterface intf: Collections.list(interfaces)) {
				for(InetAddress address: Collections.list(intf.getInetAddresses())) {
					if(!(address instanceof Inet6Address)) {
						continue;
					}
					Inet6Address addr = (Inet6Address)address;
					if(addresses.contains(addr)) {
						continue;
					}
					if(addr.isLinkLocalAddress()) {
						continue;
					}
					HashSet<Inet6Address> uncoloredAddresses = addressesByColor.get(0);
					if(uncoloredAddresses == null) {
						uncoloredAddresses = new HashSet<Inet6Address>();
					}
					uncoloredAddresses.add(addr);
					addressesByColor.put(0, uncoloredAddresses);
					addresses.add(addr);
				}
			}
		} catch (SocketException e) {
			Log.w(TAG, "Unable to enumerate network interfaces", e);
		}

		return addressesByColor;
	}

	private String jsonEncodeAddresses(SparseArray<HashSet<Inet6Address>> addresses) {
		JSONStringer encoder = new JSONStringer();
		try {
			encoder.object();
			for(int i = 0; i < addresses.size(); i++) {
				encoder.key(Integer.toString(addresses.keyAt(i)));
				encoder.array();
				for(Inet6Address address: addresses.valueAt(i)) {
					String hostAddress = address.getHostAddress();
					hostAddress = hostAddress.split("%")[0];
					encoder.value(hostAddress);
				}
				encoder.endArray();
			}
			encoder.endObject();
			return "addresses=" + encoder.toString();
		} catch(JSONException e) {
			Log.e(TAG, "Unable to encode addresses", e);
			return null;
		}
	}
}
