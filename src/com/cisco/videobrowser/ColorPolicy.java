package com.cisco.videobrowser;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class ColorPolicy {

	private static final String TAG = "VideoBrowser.ColorPolicy";	
	private static final String ns = null;

	private final LinkedList<Mapping> policy;

	private static class Mapping {
		Pattern pattern;
		int color;

		Mapping(XmlPullParser parser) throws IOException, XmlPullParserException {
			parser.require(XmlPullParser.START_TAG, ns, "mapping");
			while (parser.next() != XmlPullParser.END_TAG) {
				if (parser.getEventType() != XmlPullParser.START_TAG) {
					continue;
				}
				String name = parser.getName();
				if (name.equals("uri")) {
					readPattern(parser);
				} else if (name.equals("color")) {
					readColor(parser);
				} else {
					XmlParserUtils.skip(parser);
				}
			}
		}

		void readPattern(XmlPullParser parser) throws IOException, XmlPullParserException {
			String stringPattern = XmlParserUtils.readTextFromTag(parser, ns, "uri");
			stringPattern = stringPattern.replace(".", "\\.");
			stringPattern = stringPattern.replace("?", "\\?");
			stringPattern = stringPattern.replace("*", ".*");
			pattern = Pattern.compile(stringPattern);
		}

		void readColor(XmlPullParser parser) throws XmlPullParserException, IOException {
			color = XmlParserUtils.readIntFromTag(parser, ns, "color");
		}
	}

	private void parseXmlPolicy(InputStream in) throws XmlPullParserException, IOException {
		XmlPullParser parser = Xml.newPullParser();
		parser.setInput(in, null);

		while(parser.getEventType() != XmlPullParser.START_TAG) {
			parser.next();
		}
		parser.require(XmlPullParser.START_TAG, ns, "mappings");
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			if (name.equals("mapping")) {
				policy.add(new Mapping(parser));
			} else {
				XmlParserUtils.skip(parser);
			}
		}
	}

	public static ColorPolicy currentPolicy;

	public ColorPolicy(URL url, String encodedAddresses) throws IOException, XmlPullParserException {
		policy = new LinkedList<Mapping>();
		HttpURLConnection connection = null;
		try {
			connection = (HttpURLConnection)url.openConnection();
		} catch(IOException e) {
			Log.e(TAG, "Unable to open connection to policy server", e);
			throw e;
		}

		try {
			byte[] postData = encodedAddresses.getBytes();

			connection.setDoOutput(true);
			connection.setFixedLengthStreamingMode(postData.length);

			connection.getOutputStream().write(postData);
			connection.getOutputStream().close();

			parseXmlPolicy(connection.getInputStream());
		} catch (IOException e) {
			Log.w(TAG, "Unable to obtain policy", e);
			throw e;
		} catch (XmlPullParserException e) {
			Log.w(TAG, "Unable to parse policy", e);
			throw e;
		} finally {
			connection.disconnect();
		}
	}
	
	public int getColorForUri(String uri) throws URISyntaxException {
		return getColorForUri(new URI(uri));
	}
	
	public int getColorForUri(final URI uri) throws URISyntaxException {
		for(Mapping mapping: policy) {
			Matcher matcher = mapping.pattern.matcher(uri.toString());
			if(matcher.matches()) {
				return mapping.color;
			}
		}
		return 0;
	}

	public String applyPolicy(String uri) throws URISyntaxException {
		return applyPolicy(new URI(uri)).toString();
	}

	public URI applyPolicy(final URI uri) throws URISyntaxException {
		if(uri.getHost().contains("%")) {
			// A scope id is already given in the URI. Do not overwrite it.
			return uri;
		}
		int color = getColorForUri(uri);
		if(color == 0)
			return uri;
		else
			return new URI(uri.getScheme(), uri.getUserInfo(), uri.getHost() + "%" + Integer.toString(color),
					uri.getPort(), uri.getPath(), uri.getQuery(), uri.getFragment());
	}
}
