package com.cisco.videobrowser;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class XmlParserUtils {
	/**
	 * Skips the current tag.
	 * @param parser
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	public static void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
		if (parser.getEventType() != XmlPullParser.START_TAG) {
			throw new IllegalStateException();
		}
		int depth = 1;
		while (depth != 0) {
			switch (parser.next()) {
			case XmlPullParser.END_TAG:
				depth--;
				break;
			case XmlPullParser.START_TAG:
				depth++;
				break;
			}
		}
	}
	
	/**
	 * Returns the text in the current tag.
	 * @param parser
	 * @return
	 * @throws IOException
	 * @throws XmlPullParserException
	 */
	public static String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
	    String result = "";
	    if (parser.next() == XmlPullParser.TEXT) {
	        result = parser.getText();
	        parser.nextTag();
	    }
	    return result;
	}
	
	public static String readTextFromTag(XmlPullParser parser, String ns, String tag) throws IOException, XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, ns, tag);
		String text = readText(parser);
		parser.require(XmlPullParser.END_TAG, ns, tag);
		return text;
	}
	
	/**
	 * Returns the number in the current tag.
	 * @param parser
	 * @return
	 * @throws IOException
	 * @throws XmlPullParserException
	 * @throws NumberFormatException
	 */
	public static int readInt(XmlPullParser parser) throws IOException, XmlPullParserException, NumberFormatException {
		return Integer.parseInt(readText(parser));
	}
	
	public static int readIntFromTag(XmlPullParser parser, String ns, String tag) throws IOException, XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, ns, tag);
		int value= readInt(parser);
		parser.require(XmlPullParser.END_TAG, ns, tag);
		return value;
	}
}
